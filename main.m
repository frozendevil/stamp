//
//  main.m
//  Stamp
//
//  Created by Isadore Fraimow on 2/28/08.
//  Copyright Anathema Calculus 2008. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
