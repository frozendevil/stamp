//
//  HelperController.h
//  Stamp
//
//  Created by Isadore Fraimow on 3/8/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SRRecorderControl.h"

@class FDPopupWindow;
@class SRRecorderControl;
@class StampController;
@class PTHotKey;

@interface HelperController : NSObject <NSWindowDelegate> {
	IBOutlet NSView* oHelperView;
	IBOutlet NSSlider* oSlider;
	IBOutlet NSMatrix* oLeftRight;

	BOOL _left;
	
	IBOutlet SRRecorderControl* oShortcutRecorder;
	IBOutlet StampController* oMainController;

	FDPopupWindow *attachedWindow;
	PTHotKey *globalHotKey;
}

-(IBAction)quit:(id)sender;
-(void)openWindowAtPoint:(NSPoint)point;
-(void)hideWindow;

-(void)hitHotKey;
@end
