//
//  StampController.m
//  Stamp
//
//  Created by Isadore Fraimow on 2/28/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import "StampController.h"
#import "HelperController.h"


@implementation StampController

-(void)awakeFromNib;
{	
	_iTunes = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
	
	[[NSDistributedNotificationCenter defaultCenter] addObserver:self
														selector:@selector(iTunesUpdated:)
															name:@"com.apple.iTunes.playerInfo"
														  object:nil];
							  
	_screen = [[NSScreen mainScreen] frame];
	
	
	_left = NO;
	
	[self iTunesUpdated:nil];
}

-(void)dealloc;
{
	[[NSDistributedNotificationCenter defaultCenter] removeObserver:self];
	
	[_iTunes release];
	_iTunes = nil;
	[super dealloc];
}

-(void)displayTrackInfo;
{	
	iTunesTrack* currentTrack = [_iTunes currentTrack];

	if ( currentTrack == nil ) {
		[self noSongPlaying];		
		return;
	}
	
	[oSong setStringValue:[currentTrack name]];
	[oAlbum setStringValue:[currentTrack album]];
	[oArtist setStringValue:[currentTrack artist]];
	
	double songWidth = [[oSong attributedStringValue] size].width;
	double albumWidth = [[oAlbum attributedStringValue] size].width;
	double artistWidth = [[oArtist attributedStringValue] size].width;
	
	double longest = fmax(songWidth, albumWidth);
	longest = fmax(longest, artistWidth);
	
	NSRect newFrame;
	
	if(!_left)
		newFrame = NSMakeRect(_screen.size.width - (longest + 90 + 10 + 40), 0, longest + 90 + 10 + 40, [oWindow frame].size.height);
	else
		newFrame = NSMakeRect(0, 0, longest + 90 + 10 + 40, [oWindow frame].size.height);
	
	[[oWindow animator] setFrame:newFrame display:YES];
	
	iTunesArtwork *artwork = (iTunesArtwork *)[[[currentTrack artworks] get] lastObject];
	
	NSImage *artworkImage;
	
	if ( artwork == nil ) 
		artworkImage = [NSImage imageNamed:@"nocover"];
	else
		artworkImage = [artwork data];
	
	[oAlbumArtView setImage:artworkImage];
	
	[oWindow viewsNeedDisplay];
	
	//[artwork release];
}

-(void)noSongPlaying;
{	
	[oSong setStringValue:@""];
	[oAlbum setStringValue:@"No Song Playing"];
	[oArtist setStringValue:@""];
		
	[oAlbumArtView setImage:nil];
	
	double albumWidth = [[oAlbum attributedStringValue] size].width;
	NSRect newFrame;
	
	if(!_left)
		newFrame = NSMakeRect(_screen.size.width - (albumWidth + 40), 0, albumWidth + 40, [oWindow frame].size.height);
	else
		newFrame = NSMakeRect(0, 0, albumWidth + 44, [oWindow frame].size.height);
	
	[[oWindow animator] setFrame:newFrame display:YES];
}

-(void)playpause;
{
	if (_iTunes == nil || ![_iTunes isRunning]) return;
	[_iTunes playpause];
}


-(void)iTunesUpdated:(NSNotification *)notification;
{
	if (_iTunes == nil || 
		[[[notification userInfo] objectForKey:@"Player State"] isEqualToString:@"Stopped"] ||
		[_iTunes playerState] == iTunesEPlSStopped ||
		![_iTunes isRunning] ) {
			[self noSongPlaying];
	} else {
		[self displayTrackInfo];
	}
}

-(NSNumber *)getTextWidthFromTextField:(NSTextField *)theField;
{
	return [NSNumber numberWithFloat:[[theField attributedStringValue] size].width];
}

-(void)switchSides;
{
	_left = !_left;
	if(_left) {
		[oSong setAlignment:NSLeftTextAlignment];
		[oAlbum setAlignment:NSLeftTextAlignment];
		[oArtist setAlignment:NSLeftTextAlignment];
		
		[oAlbumArtView setFrameOrigin:NSMakePoint([oWindow frame].size.width - 20 - 90, 5)];
		[oAlbumArtView setAutoresizingMask: (NSViewMinXMargin | NSViewMinYMargin | NSViewMaxYMargin)];
	} else {
		[oSong setAlignment:NSRightTextAlignment];
		[oAlbum setAlignment:NSRightTextAlignment];
		[oArtist setAlignment:NSRightTextAlignment];
		
		[oAlbumArtView setFrameOrigin:NSMakePoint(20, 5)];
		[oAlbumArtView setAutoresizingMask: (NSViewMaxXMargin | NSViewMinYMargin | NSViewMaxYMargin)];
	}
	
	[self iTunesUpdated:nil];
}

-(void)popUpHelperWindowAtPoint:(NSPoint)point;
{
	[helper openWindowAtPoint:point];
}

-(void)setWindowAlphaValue:(float)alpha;
{
	[oWindow setAlphaValue:alpha];
}

-(IBAction)sliderMoved:(id)sender;
{
	[oWindow setAlphaValue:[oSlider floatValue]];
}

@end
