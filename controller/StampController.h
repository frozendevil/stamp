//
//  StampController.h
//  Stamp
//
//  Created by Isadore Fraimow on 2/28/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import "FDAlbumArtView.h"
#import "StampWindow.h"
#import "iTunes.h"


@class iTunesApplication;
@class HelperController;

@interface StampController : NSObject {
	IBOutlet FDAlbumArtView* oAlbumArtView;
	
	IBOutlet NSTextField* oArtist;
	IBOutlet NSTextField* oAlbum;
	IBOutlet NSTextField* oSong;
	IBOutlet StampWindow* oWindow;
	IBOutlet HelperController* helper;
	IBOutlet NSSlider* oSlider;
	
	iTunesApplication* _iTunes;
	
	NSRect _screen;
	
	BOOL _left;
}

-(void)displayTrackInfo;
-(void)noSongPlaying;

-(void)playpause;

-(void)iTunesUpdated:(NSNotification *)notification;

-(NSNumber *)getTextWidthFromTextField:(NSTextField* )theField;

-(void)switchSides;

-(void)popUpHelperWindowAtPoint:(NSPoint)point;
-(void)setWindowAlphaValue:(float)alpha;

-(IBAction)sliderMoved:(id)sender;

@end
