//
//  HelperController.m
//  Stamp
//
//  Created by Isadore Fraimow on 3/8/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import "HelperController.h"
#import "StampController.h"
#import "SRRecorderControl.h"
#import "SRCommon.h"
#import "FDPopupWIndow.h"
#import "PTHotKeyCenter.h"
#import "PTHotKey.h"


@implementation HelperController

-(void)awakeFromNib;
{
	[oShortcutRecorder setCanCaptureGlobalHotKeys:YES];
	[NSApp setDelegate:self];
}

-(IBAction)quit:(id)sender;
{
	[NSApp terminate:self];
}

-(void)openWindowAtPoint:(NSPoint)point;
{
	if(attachedWindow) {
		[attachedWindow openAtPoint:point];
		[attachedWindow makeKeyWindow];

		return;
	}
	
	attachedWindow = [[FDPopupWindow alloc] initWithView:oHelperView 
												 atPoint:point];
	[attachedWindow setDelegate:self];

	[attachedWindow makeKeyAndOrderFront:self];
	[attachedWindow makeKeyWindow];
}

-(void)hideWindow;
{
	[attachedWindow orderOut:self];
	
	BOOL left = ([oLeftRight selectedColumn] == 0);
	
	if(left != _left) [oMainController switchSides];
	
	_left = left;
	
	[[NSUserDefaults standardUserDefaults] setBool:left forKey: @"isLeft"];
}

- (void)windowDidResignKey:(NSNotification *)notification;
{
	[self hideWindow];
}

- (BOOL)shortcutRecorder:(SRRecorderControl *)aRecorder 
			   isKeyCode:(NSInteger)keyCode 
		   andFlagsTaken:(NSUInteger)flags 
				  reason:(NSString **)aReason;
{
	return NO;
}

- (void)shortcutRecorder:(SRRecorderControl *)aRecorder keyComboDidChange:(KeyCombo)newKeyCombo;
{
	if (globalHotKey != nil)
	{
		[[PTHotKeyCenter sharedCenter] unregisterHotKey: globalHotKey];
		[globalHotKey release];
		globalHotKey = nil;
	}

	globalHotKey = [[PTHotKey alloc] initWithIdentifier:@"StampKey"
											   keyCombo:[PTKeyCombo keyComboWithKeyCode:[oShortcutRecorder keyCombo].code
																			  modifiers:[oShortcutRecorder cocoaToCarbonFlags:[oShortcutRecorder keyCombo].flags]]];
	
	[globalHotKey setTarget: self];
	[globalHotKey setAction: @selector(hitHotKey)];
	
	[[PTHotKeyCenter sharedCenter] registerHotKey: globalHotKey];
}

-(void)hitHotKey;
{
	[oMainController playpause];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
	id keyComboPlist;
	PTKeyCombo* keyCombo = nil;
	
	keyComboPlist = [[NSUserDefaults standardUserDefaults] objectForKey: @"stampKeyCombo"];
	keyCombo = [[[PTKeyCombo alloc] initWithPlistRepresentation: keyComboPlist] autorelease];

	[oShortcutRecorder setKeyCombo:SRMakeKeyCombo([keyCombo keyCode], [oShortcutRecorder carbonToCocoaFlags:[keyCombo modifiers]])];

	float prefAlpha = [[NSUserDefaults standardUserDefaults] floatForKey: @"windowAlpha"];
	
	if(isnan(prefAlpha) || prefAlpha < 0.5 ) prefAlpha = 0.6;
	[oMainController setWindowAlphaValue:prefAlpha];
	[oSlider setFloatValue:prefAlpha];
	
	_left = [[NSUserDefaults standardUserDefaults] boolForKey: @"isLeft"];
	if(_left) {
		[oLeftRight selectCellAtRow:0 column:0];
		[oMainController switchSides];
	}
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	[[NSUserDefaults standardUserDefaults] setObject:[[globalHotKey keyCombo] plistRepresentation] forKey:@"stampKeyCombo"];
	[[NSUserDefaults standardUserDefaults] setFloat:[oSlider floatValue] forKey:@"windowAlpha"];
	
	BOOL left = ([oLeftRight selectedColumn] == 0);
	[[NSUserDefaults standardUserDefaults] setBool: left forKey: @"isLeft"];

	
	[[PTHotKeyCenter sharedCenter] unregisterHotKey: globalHotKey];

	[globalHotKey release];
	globalHotKey = nil;
}

@end
