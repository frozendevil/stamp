//
//  StampWindow.h
//  Stamp
//
//  Created by Isadore Fraimow on 2/28/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class StampController;
@interface StampWindow : NSWindow {
	IBOutlet StampController* controller;
}

@end
