//
//  EffDee.m
//  Stamp
//
//  Created by Isadore Fraimow on 3/8/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import "EffDee.h"


@implementation EffDee
-(void)awakeFromNib;
{
	[self setImage:[NSImage imageNamed:@"effdee"]];
}

@end
