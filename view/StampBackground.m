//
//  StampBackground.m
//  Stamp
//
//  Created by Isadore Fraimow on 2/28/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import "StampBackground.h"


@implementation StampBackground

-(void)awakeFromNib
{
    [self setNeedsDisplay:YES];
}

-(void)drawRect:(NSRect)rect
{	
	[[NSColor blackColor] set];
	
	NSRectFill([self frame]);
		
	NSBezierPath *thePath = [NSBezierPath bezierPath];
	if([self window].frame.origin.x > [[NSScreen mainScreen] frame].size.width/2) {
		[thePath moveToPoint:[self frame].origin];
		[thePath lineToPoint:NSMakePoint([self frame].origin.x, [self frame].origin.y + [self frame].size.height)];
		[thePath lineToPoint:NSMakePoint([self frame].origin.x + [self frame].size.width, [self frame].origin.y + [self frame].size.height)];
	} else {
		[thePath moveToPoint:NSMakePoint([self frame].origin.x, [self frame].origin.y + [self frame].size.height)];
		[thePath lineToPoint:NSMakePoint([self frame].origin.x + [self frame].size.width, [self frame].origin.y + [self frame].size.height)];
		[thePath lineToPoint:NSMakePoint([self frame].origin.x + [self frame].size.width, [self frame].origin.y)];
	}
	
	[[NSColor whiteColor] set];
	
	[thePath setLineWidth:2];
	[thePath stroke];
	
	[[self window] setHasShadow:NO];
	[[self window] setHasShadow:YES];
}

@end
