//
//  StampWindow.m
//  Stamp
//
//  Created by Isadore Fraimow on 2/28/08.
//  Copyright 2008 Anathema Calculus. All rights reserved.
//

#import "StampWindow.h"
#import "StampController.h"
#import <AppKit/AppKit.h>

@implementation StampWindow

- (id)initWithContentRect:(NSRect)contentRect styleMask:(unsigned int)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {

    self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    [self setBackgroundColor: [NSColor clearColor]];
	[self setMovableByWindowBackground:NO];
    [self setLevel: kCGDesktopWindowLevel + 1];
	//[self setExcludedFromWindowsMenu:YES];
    [self setAlphaValue:0.6];
    [self setOpaque:NO];
    [self setHasShadow: YES];
    return self;
}

-(void)sendEvent:(NSEvent *)theEvent;
{
	if([theEvent type] == NSRightMouseDown) {
		NSRect frame = [self frame];
		NSPoint attachedWindowPoint = NSMakePoint(NSMidX(frame), NSMaxY(frame));
		
		[controller popUpHelperWindowAtPoint:attachedWindowPoint];
	}
	
	[super sendEvent:theEvent];
}

@end
