//
//  FDPopupWindow.m
//  FDPopupWindow
//
//  Created by Isadore Fraimow on 3/15/08.
//

#import "FDPopupWindow.h"

#define ARROW_HEIGHT 16.0
#define ARROW_WIDTH 20.0
#define MARGIN 2.0
#define BACKGROUND_COLOR [NSColor colorWithCalibratedWhite:1.0 alpha:0.95]
#define CORNER_RADIUS 8.0


@implementation FDPopupWindow
- (FDPopupWindow *)initWithView:(NSView *)view atPoint:(NSPoint)point;
{
	if(!view) return nil;
	
	NSRect contentRect = NSZeroRect;
    contentRect.size = [view frame].size;
    
    if (!(self = [super initWithContentRect:contentRect 
								  styleMask:NSBorderlessWindowMask
									backing:NSBackingStoreBuffered
									  defer:NO])) return nil;
	_view = view;
	_point = point;
	
	[super setBackgroundColor:[NSColor clearColor]];
	[self setMovableByWindowBackground:NO];
	[self setExcludedFromWindowsMenu:YES];
	[self setAlphaValue:1.0];
	[self setOpaque:NO];
	[self setHasShadow:YES];
	[self useOptimizedDrawing:YES];
	
	[self makeFrame];
	[self drawBackground];

	[[self contentView] addSubview:_view];
	
	return self;
}


- (void)openAtPoint:(NSPoint)point;
{
	_point = point;
	
	[self makeFrame];
	[self drawBackground];
	
	[self makeKeyAndOrderFront:self];
}


- (void)makeFrame;
{
	NSRect contentRect = NSZeroRect;
    contentRect.size = [_view frame].size;
		
	contentRect = NSInsetRect(contentRect, -MARGIN, -MARGIN - (ARROW_HEIGHT/2.0));
	
	float halfWidth = contentRect.size.width / 2.0;
	
	contentRect.origin = NSMakePoint(_point.x - halfWidth, _point.y + 1.0);
    
    _viewFrame = NSMakeRect(MARGIN, 
							MARGIN + ARROW_HEIGHT, 
							[_view frame].size.width, 
							[_view frame].size.height);
	

	contentRect = [self boundToScreen:contentRect];
		
	[self setFrame:contentRect display:NO];
	[_view setFrame:_viewFrame];
}

- (NSRect)boundToScreen:(NSRect)rect;
{
	float screenWidth = [[NSScreen mainScreen] frame].size.width;
	
	if(NSMaxX(rect) > screenWidth)
		rect.origin.x -= NSMaxX(rect) - screenWidth;
	if(rect.origin.x < 0)
		rect.origin.x = 0;
	
	return rect;
}


- (void)drawBackground;
{
    NSDisableScreenUpdates();
    [super setBackgroundColor:[self backgroundColorPatternImage]];
    if ([self isVisible]) {
        [self display];
        [self invalidateShadow];
    }
    NSEnableScreenUpdates();
}


- (NSColor *)backgroundColorPatternImage;
{
    NSImage *background = [[NSImage alloc] initWithSize:[self frame].size];
    NSRect backgroundRect = NSZeroRect;
    backgroundRect.size = [background size];
    
    [background lockFocus];
		NSBezierPath *backgroundPath = [self backgroundPath];
		[NSGraphicsContext saveGraphicsState];
		[backgroundPath addClip];
		
		[BACKGROUND_COLOR set];
		[backgroundPath fill];
		
		[NSGraphicsContext restoreGraphicsState];
    [background unlockFocus];
    
    return [NSColor colorWithPatternImage:[background autorelease]];
}

- (NSBezierPath *)backgroundPath;
{
	NSBezierPath *path = [NSBezierPath bezierPath];
    [path setLineJoinStyle:NSRoundLineJoinStyle];
	
	NSRect contentArea = NSInsetRect(_viewFrame, -MARGIN, -MARGIN);
	
	path = [NSBezierPath bezierPathWithRoundedRect:contentArea xRadius:CORNER_RADIUS yRadius:CORNER_RADIUS];
		
	NSBezierPath *arrow = [NSBezierPath bezierPath];
	[arrow setLineJoinStyle:NSRoundLineJoinStyle];
	
	NSPoint arrowTip = NSMakePoint(NSMaxX([self frame]) - _point.x, 1);
	
	[arrow moveToPoint:arrowTip];
	[arrow lineToPoint:NSMakePoint(arrowTip.x - (ARROW_WIDTH / 2.0), ARROW_HEIGHT)];
	[arrow lineToPoint:NSMakePoint(arrowTip.x + (ARROW_WIDTH / 2.0), ARROW_HEIGHT)];
	[arrow moveToPoint:arrowTip];
	[arrow closePath];
	
	[path appendBezierPath:arrow];
	
	return path;
}

# pragma mark NSWindow methods

- (BOOL)canBecomeMainWindow
{
    return NO;
}


- (BOOL)canBecomeKeyWindow
{
    return YES;
}


- (BOOL)isExcludedFromWindowsMenu
{
    return YES;
}

@end
