//
//  FDPopupWindow.h
//  FDPopupWindow
//
//  Created by Isadore Fraimow on 3/15/08.
//

#import <Cocoa/Cocoa.h>


@interface FDPopupWindow : NSWindow {
	__weak NSView* _view;
	NSPoint _point;
	NSRect _viewFrame;
}

- (FDPopupWindow *)initWithView:(NSView *)view atPoint:(NSPoint)point;
- (void)openAtPoint:(NSPoint)point;

- (NSRect)boundToScreen:(NSRect)rect;

- (void)makeFrame;
- (void)drawBackground;
- (NSColor *)backgroundColorPatternImage;
- (NSBezierPath *)backgroundPath;

@end
